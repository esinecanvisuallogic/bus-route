# Bus Route #

GoEuro's Bus Route challenge. 

I'm not putting any building and running instructions because it is compliant with the spec.

### What is mildly interesting about the project ###

- I have a "restartUsingEndpoint" property in the application.yml file. If set to true, it'll restart the application using spring boot actuator's restart endpoint. Otherwise, it'll restart using the context. Right now it's set to false, so restart is done via context.
- I have done two implementations. One loads the file lazily, the other loads eagerly. If you just use the endpoint as specified in the specs, you'll be using the eager implementation. If you set the optional parameter "service_type" as "B", you'll be using the lazy one.
- The app generates a Logging.html file at the root when it's starting up. I'd recommend keeping it open on a browser window. It tells about the state of the app in real time. Sometimes useful.