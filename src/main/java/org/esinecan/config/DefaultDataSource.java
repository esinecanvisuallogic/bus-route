package org.esinecan.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@Configuration
public class DefaultDataSource {

    @Value("${defaultDataSource}")
    private String defaultDataSourceFileName;

    @Bean
    public String getPath(){
        return DefaultDataSource.class.getClassLoader().getResource(defaultDataSourceFileName).getPath();
    }
}
