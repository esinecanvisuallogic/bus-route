package org.esinecan.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * Probably test cases will not have a lot of repeating requests but in a real world scenario, people would probably
 * querying the same travels around same hours. Due to national holidays, weekends, approaching departures, etc.
 *
 * This would benefit BufferedReader approach more than the LinkedHashSet approach.
 *
 * Created by eren.sinecan on 04.05.17.
 */
@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache("routesbuff"),
                new ConcurrentMapCache("routesset")));
        return cacheManager;
    }
}
