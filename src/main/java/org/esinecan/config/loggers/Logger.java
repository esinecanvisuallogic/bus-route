package org.esinecan.config.loggers;

import org.esinecan.config.loggers.formatters.HtmlLogFormatter;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by eren.sinecan on 04.05.17.
 */
public class Logger {

    private static FileHandler fileTxt;
    private static SimpleFormatter formatterTxt;

    private static FileHandler fileHTML;
    private static Formatter formatterHTML;

    public static java.util.logging.Logger LOGGER =
            java.util.logging.Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);

    /**
     * We attach handlers for our Html and Text formatters. Text formatter is the default
     * SimpleFormatter, Html has custom org.esinecan.config.loggers.formatters.HtmlLogFormatter
     *
     * @throws IOException
     */
    static {

        LOGGER.setLevel(Level.INFO);
        try {
            fileTxt = new FileHandler("Logging.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            fileHTML = new FileHandler("Logging.html");
        } catch (IOException e) {
            e.printStackTrace();
        }

        formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        LOGGER.addHandler(fileTxt);

        java.util.logging.Logger rootLogger = java.util.logging.Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            handlers[0].setFormatter(formatterTxt);
        }

        // create an HTML formatter
        formatterHTML = new HtmlLogFormatter();
        fileHTML.setFormatter(formatterHTML);
        LOGGER.addHandler(fileHTML);

    }
}
