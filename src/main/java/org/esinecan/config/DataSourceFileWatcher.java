package org.esinecan.config;

import io.reactivex.Single;
import org.esinecan.service.util.RestartApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;

import static java.util.Objects.isNull;
import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * This class is responsible of locating and watching the data source file.
 *
 * Created by eren.sinecan on 04.05.17.
 */
@Configuration
public class DataSourceFileWatcher {

    @Autowired
    private DefaultDataSource defaultDataSource;

    @Autowired
    private RestartApplicationService restartApplicationService;

    public static String DATA_SOURCE_PATH; //FIXME: Not very nice

    @PostConstruct
    public void init() throws FileNotFoundException {
        fallBackToDefaultDataSourceIfNeeded();
        watchDataSourceFile();
    }

    /**
     * If no data source was specified at all, uses the default. If an address was provided, first treats it as an
     * absolute path. If the file was not found, treats the address as a resource. If it still can't find it, logs and
     * shuts down.
     */
    private void fallBackToDefaultDataSourceIfNeeded() throws FileNotFoundException {
        if(isNull(DATA_SOURCE_PATH)){
            LOGGER.warning("No data sources were passed. Falling back to default");
            DATA_SOURCE_PATH = defaultDataSource.getPath();
        }else{
            File f = new File(DATA_SOURCE_PATH);
            LOGGER.info("Looking for the file at: " + DATA_SOURCE_PATH);
            if(!f.exists() || f.isDirectory()) {
                try{
                    URL relativeR = DataSourceFileWatcher.class.getClassLoader().getResource(DATA_SOURCE_PATH);
                    LOGGER.warning("Not found. Looking for the file at: " + relativeR.getPath());
                    File relativeF = new File(relativeR.getPath());
                    if(!relativeF.exists() || relativeF.isDirectory()) {
                        throw new FileNotFoundException("No data sources were found");
                    }
                    DATA_SOURCE_PATH = relativeR.getPath();
                }catch (Exception ex){
                    LOGGER.log(SEVERE, "HOW TO PROVIDE A DATA SOURCE: \n This application;\n" +
                            "- If no data source was specified at all, uses the default.\n" +
                            "- If an address was provided, first treats it as an absolute path.\n" +
                            "- If the file was not found, treats the address as a resource.\n" +
                            "- If it still can't find it, logs and shuts down.\n" +
                            "- Exception from your last attempt: ", ex);
                    throw ex;
                }
            }
        }
    }

    /**
     * This method watches over the data source file on a separate thread so that the waiting of WatchService.take()
     * will not cause application to stop.
     */
    @CacheEvict(cacheNames = {"routesbuff", "routesset"}, allEntries = true)
    public void watchDataSourceFile(){
        final Path path = FileSystems.getDefault().getPath(DATA_SOURCE_PATH);
        final WatchService watchService;
        try {
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            LOGGER.log(SEVERE, "WatchService creation failed. Changes to data source file will not be loaded: ", e);
            return;
        }
        final WatchKey key;
        try {
            key = path.getParent().register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            LOGGER.log(SEVERE, "Data source file could not be registered with the WatchService. " +
                    "Changes to data source file will not be loaded: ", e);
            return;
        }
        Single.create(singleEmitter -> {
            Thread thread = new Thread(() -> {
                try {
                    LOGGER.info("Watching: " + path.getParent().toUri().toString());
                    WatchKey wk;
                    wk = watchService.take();
                    LOGGER.info("Watcher detected: " + wk.toString());
                    if(wk.pollEvents().stream().anyMatch(event ->
                            event.context().toString().equals(path.getFileName().toString()))){
                        wk.cancel();
                        key.cancel();
                        LOGGER.info("File changed. Restarting...");
                        String[] args = {DATA_SOURCE_PATH};
                        restartApplicationService.reRun(args);
                    }
                }catch (Exception e){
                    singleEmitter.onError(e);
                }
            });
            thread.start();
        }).doOnError(throwable ->
                LOGGER.log(SEVERE, "Source file watching was interrupted: ", throwable)
        ).doFinally(() ->
                LOGGER.info("Watchers have been configured.")
        ).subscribe() //We're not very interested in what this will emit aside from logging purposes.
                .dispose();
    }
}
