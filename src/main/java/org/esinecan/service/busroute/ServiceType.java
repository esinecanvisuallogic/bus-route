package org.esinecan.service.busroute;

import lombok.Getter;

import java.util.Arrays;

/**
 * Just an Enum to help us decide which implementation of BusRouteCheckService is called
 *
 * Created by eren.sinecan on 04.05.17.
 */
public enum ServiceType {
    BUFF("B"),
    SET("S");

    @Getter
    private String value;

    private ServiceType(String value){
        this.value = value;
    }

    /**
     * Naturally, this is slower than the usual implementation of Enum getByValues, where a map is declared as a member
     * variable, and it is populated inside a static{...} function so that it can be easily queried at runtime. But I
     * don't see the reason to clutter the place up when we have only two values.
     *
     * @param value String representation of the Enum.
     * @return
     */
    public static ServiceType getByValue(String value){
        return Arrays.stream(ServiceType.values()).filter(
                        serviceType -> serviceType.getValue().equals(value)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", value)));
    }
}
