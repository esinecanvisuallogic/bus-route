package org.esinecan.service.busroute.declaration;

import org.esinecan.model.BusRoute;

import java.io.IOException;

/**
 * Created by eren.sinecan on 04.05.17.
 */
public interface BusRouteCheckService {
    BusRoute hadDirectRoute(Integer depSid, Integer arrSid) throws IOException;
}
