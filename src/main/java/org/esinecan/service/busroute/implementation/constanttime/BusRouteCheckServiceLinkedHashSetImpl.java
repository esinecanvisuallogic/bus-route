package org.esinecan.service.busroute.implementation.constanttime;

import org.esinecan.model.BusRoute;
import org.esinecan.service.busroute.declaration.BusRouteCheckService;
import org.esinecan.service.busroute.implementation.constanttime.reader.ConstantTimeRouteReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@Service
@Qualifier("BusRouteCheckServiceLinkedHashSet")
@CacheConfig(cacheNames={"routesset"})
public class BusRouteCheckServiceLinkedHashSetImpl implements BusRouteCheckService {

    @Autowired
    private ConstantTimeRouteReader constantTimeRouteReader;

    /**
     * This method simply creates an object where the fields included in the hashcode generation are same as the one
     * that is in the Set (That is, of course, if there is one) and checks if the equals method returns true.
     * @param depSid
     * @param arrSid
     * @return BusRoute object with the directBusRoute info
     */
    @Override
    @Cacheable
    public BusRoute hadDirectRoute(Integer depSid, Integer arrSid) {
        BusRoute busRoute = new BusRoute(depSid, arrSid, null);
        busRoute.setDirectBusRoute(constantTimeRouteReader.routes.contains(busRoute) ? true : false);
        LOGGER.info("Linear time service claims that: " + busRoute);
        return busRoute;
    }
}
