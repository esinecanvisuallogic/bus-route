package org.esinecan.service.busroute.implementation.lineartime.reader;

import org.springframework.stereotype.Component;

import java.io.*;

import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.DataSourceFileWatcher.DATA_SOURCE_PATH;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@Component
public class LinearTimeRouteReader {

    /**
     * Returns the buffered reader at the beginning of the data source so that we can use it to traverse the whole thing
     * @return BufferedReader with source's stream.
     * @throws FileNotFoundException
     */
    public BufferedReader getReader() throws FileNotFoundException {
        File routeFile = new File(DATA_SOURCE_PATH);
        InputStream inputFS = null;
        try {
            inputFS = new FileInputStream(routeFile);
        } catch (FileNotFoundException e) {
            LOGGER.log(SEVERE, "Dreadful stuff occured. We can't find the data source: ", e);
            throw e;
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputFS));
        return reader;
    }
}
