package org.esinecan.service.busroute.implementation.lineartime;

import org.esinecan.model.BusRoute;
import org.esinecan.service.busroute.declaration.BusRouteCheckService;
import org.esinecan.service.busroute.implementation.lineartime.reader.LinearTimeRouteReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;

import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@Service
@Qualifier("BusRouteCheckServiceBufferedReader")
@CacheConfig(cacheNames={"routesbuff"})
public class BusRouteCheckServiceBufferedReaderImpl implements BusRouteCheckService {

    @Autowired
    private LinearTimeRouteReader routeReader;

    /**
     * Sequentially reads the data source until it encounters a match. If it does not, it traverses the whole thing in
     * vain. At any given moment, it does not consume a lot of memory. However, it might have very high time complexity
     * when there is a big data source and the route is nonexistent.
     *
     * @param depSid
     * @param arrSid
     * @return BusRoute object with the directBusRoute info
     * @throws IOException
     */
    @Cacheable
    public BusRoute hadDirectRoute(Integer depSid, Integer arrSid) throws IOException {
        String depSidStr = String.valueOf(depSid);
        String arrSidStr = String.valueOf(arrSid);

        BufferedReader reader = routeReader.getReader();
        Boolean hasRoute = reader.lines().anyMatch(
                line -> {
                    if (line.contains(" ")) {
                        String stations = line.substring(line.indexOf(" "));
                        return stations.contains(depSidStr)
                                && stations.contains(arrSidStr)
                                && stations.indexOf(depSidStr) < stations.indexOf(arrSidStr);
                    }
                    return false;
                }
        );
        try {
            reader.close();
        } catch (IOException e) {
            LOGGER.log(SEVERE, "We seem to be unable to let go of the data source after reading it: ", e);
            throw e;
        }
        BusRoute busRoute = new BusRoute(depSid, arrSid, hasRoute);
        LOGGER.info("Linear time service claims that: " + busRoute);
        return busRoute;
    }
}
