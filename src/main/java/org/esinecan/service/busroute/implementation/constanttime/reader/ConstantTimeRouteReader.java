package org.esinecan.service.busroute.implementation.constanttime.reader;

import lombok.Getter;
import lombok.experimental.Accessors;
import org.esinecan.model.BusRoute;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.DataSourceFileWatcher.DATA_SOURCE_PATH;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * This reader does not at all execute in constant time. The name is about the query time from the service.
 *
 * Created by eren.sinecan on 04.05.17.
 */
@Component
@Accessors(fluent = true)
public class ConstantTimeRouteReader {
    @Getter
    public LinkedHashSet<BusRoute> routes;

    /**
     * Parses the data source into a LinkedHashSet that contains every direct departure -> arrival route.
     *
     * @return A LinkedHashSet that has all the possible one stop routes
     * @throws FileNotFoundException
     */
    @PostConstruct
    public void readAllOneBusRoutesIntoMap() throws IOException {

        String allRoutes = getDataSourceFileAsString();

        LOGGER.info("Constant Time Route Reader is attempting to parse " + allRoutes.length()
                + " bytes at once. This might take a while.");

        routes = Arrays.stream(allRoutes.split("\n"))
                .map(String::trim).filter(this::isValidRouteLine).map(this::stripRouteId).
                map(routeLine -> Arrays.stream(getStopsFromRouteLine(routeLine)).flatMap(departure ->
                        Arrays.stream(getStopsAfterMe(String.valueOf(departure), routeLine)).map(arrival ->
                                new BusRoute(departure, arrival, null)
                        )
                )
        ).flatMap(Stream::sequential).collect(Collectors.toCollection(LinkedHashSet::new));

        LOGGER.info("Constant Time Route Reader has " + routes.size() + " direct route definitions.");
    }

    /**
     * For a trimmed line to be a valid route definition, it needs to have at least two occurences of <number><space>
     *     pattern, ending with a number again
     *
     * @param routeLine a line read from the data source
     * @return
     */
    private boolean isValidRouteLine(String routeLine){
        return routeLine.matches("(\\d+\\s*){2,}?(\\d+)");
    }

    /**
     * Given we have a valid line, this method removes the route id (i.e. first <number><space> pattern) from the route
     * definition, leaving us with only station ids.
     *
     * @param routeLine a line read from the data source
     * @return
     */
    private String stripRouteId(String routeLine){
        return routeLine.substring(routeLine.indexOf(" ")).trim();
    }

    /**
     * Extracts every station id from the route definition
     *
     * @param routeLine a line read from the data source
     * @return
     */
    private Integer[] getStopsFromRouteLine(String routeLine){
        return Arrays.stream(routeLine.split("\\s+"))
                        .map(Integer::valueOf).toArray(Integer[]::new);
    }

    /**
     * Since our route definitions are directed, we're only interested in arrival stations that occur after the
     * departure station. This method returns arrival stations for a given departure station in the route definition.
     *
     * @param depSid
     * @param routeLine
     * @return
     */
    private Integer[] getStopsAfterMe(String depSid, String routeLine){
        return routeLine.endsWith(depSid) ?
                new Integer[0] :
                Arrays.stream(routeLine.substring(routeLine.indexOf(depSid + " ")).trim().split("\\s+")).
                        map(Integer::valueOf).toArray(Integer[]::new);
    }

    /**
     * Reads the file in a single thread. Using memory mapped approach, we're utilizing the page cache that ordinarily
     * deals with page files. We'd like this particular implementation to be very fast, but we'll end up using a lot of
     * memory as we'll write all possible routes on our set to facilitate near constant time returns to queries. We
     * should have 2 to 3gb of mappable space depending on the OS kernel. This, in conjunction with the map we're
     * holding, will consume quite a lot of memory at start time, and still a sizable amount at runtime.
     *
     * @return data source as String
     * @throws IOException
     */
    private String getDataSourceFileAsString() throws IOException {
        final FileChannel channel;
        try {
            channel = new FileInputStream(DATA_SOURCE_PATH).getChannel();
        } catch (FileNotFoundException e) {
            LOGGER.log(SEVERE, "Dreadful stuff occured. We can't find the data source: ", e);
            throw e;
        }
        MappedByteBuffer buffer = null;
        try {
            buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        } catch (IOException e) {
            LOGGER.log(SEVERE, "We found the data source, but we can't read it: ", e);
            throw e;
        }
        CharBuffer charBuffer = Charset.forName(System.getProperty("file.encoding")).decode(buffer);
        String allData = charBuffer.toString();
        channel.close();
        return allData;
    }
}
