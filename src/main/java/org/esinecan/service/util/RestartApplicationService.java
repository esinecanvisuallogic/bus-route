package org.esinecan.service.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.restart.RestartEndpoint;
import org.springframework.stereotype.Service;

import static org.esinecan.Application.main;
import static org.esinecan.Application.shutDown;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@Service
public class RestartApplicationService {

    @Autowired
    private RestartEndpoint restartEndpoint;

    @Value("${restartUsingEndpoint}")
    private boolean restartUsingEndpoint;

    /**
     * This variable counts the times application has restarted
     */
    public static int restartCount = 0;

    /**
     * Depending on the restartUsingEndpoint property, restarts the app using either context or RestartEndpoint
     * @param args
     */
    public void reRun(String[] args){
        if(restartUsingEndpoint){
            reRunUsingEndpoint();
        }else {
            reRunUsingContext(args);
        }
        restartCount++;
        LOGGER.info("Total number of restarts: " + restartCount);
    }

    /**
     * Quite simply, makes calls to shutDown and main functions so that the application is turned off and turned on
     * again.
     *
     * @param args The very same "args" from the main method.
     */
    private void reRunUsingContext(String[] args){
        shutDown();
        LOGGER.info("Boot starting...");
        main(args);
        LOGGER.info("Restart complete.");
    }

    /**
     * This is a method of restarting I do not really favor because it causes memory leaks. But I'm including it for
     * reviewing purposes.
     */
    private void reRunUsingEndpoint(){
        Thread restartThread = new Thread(() -> restartEndpoint.restart());
        restartThread.setDaemon(false);
        restartThread.start();
        LOGGER.info("RestartEndpoint is called");
    }
}
