package org.esinecan.web;

import org.esinecan.model.BusRoute;
import org.esinecan.service.busroute.ServiceType;
import org.esinecan.service.busroute.declaration.BusRouteCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 03.05.17.
 */
@RestController
public class BusRouteController {

    @Autowired
    @Qualifier("BusRouteCheckServiceBufferedReader")
    private BusRouteCheckService busRouteCheckServiceBuff;

    @Autowired
    @Qualifier("BusRouteCheckServiceLinkedHashSet")
    private BusRouteCheckService busRouteCheckServiceSet;

    /**
     * This Controller method has an additional, optional parameter that was not in the specs. I implemented two
     * different solutions, which are mapped to the ServiceType enum. First solution lazily iterates over the route
     * info until it finds what it wants. The second one remembers all the possible one stop routes and picks the
     * answer in constant time.
     *
     * I wanted to implement a third one that treats the problem as a directioned unweighted graph problem, but honestly
     * I have to do some reading on Djikstra's algorithm and then modify it while not gaining much memory or time
     * complexity. So I forgo it as I'm having a pretty busy week unfortunately.
     *
     * @param depSid
     * @param arrSid
     * @param serviceType
     * @return
     */
    @RequestMapping(value = "/direct", method = RequestMethod.GET)
    public BusRoute isDirectRoute(@RequestParam("dep_sid") Integer depSid,
                                  @RequestParam("arr_sid") Integer arrSid,
                                  @RequestParam(name = "service_type", defaultValue = "S") String serviceType )
            throws IOException {

        BusRouteCheckService service;
        ServiceType type = ServiceType.getByValue(serviceType);

        switch (type) {
            case BUFF:
                LOGGER.info("Going to run the linear time route service for dep_sid: "
                        + depSid + " arr_sid: " + arrSid);
                service = busRouteCheckServiceBuff;
                break;

            case SET:
                LOGGER.info("Going to run the constant time route service for dep_sid: "
                        + depSid + " arr_sid: " + arrSid);
                service = busRouteCheckServiceSet;
                break;

            default:
                String errMsg = String.format("Unsupported service type %s.", serviceType);
                LOGGER.log(SEVERE, errMsg);
                throw new IllegalArgumentException(errMsg);
        }
        return service.hadDirectRoute(depSid, arrSid);
    }
}
