package org.esinecan.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by eren.sinecan on 03.05.17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
/**
 * EqualsAndHashCode annotation is for LinkedHashSet approach. But we exclude the directBusRoute field because
 * obviously we do not have that information on query time, so we can't build the correct hashcode if we include it.
 *
 * Anyway, Ideally, we would like that none of the other routes possess the departure and arrival stations that are
 * being queried. In such source files, we would be able to achieve O(1) time complexity. However, at the other end
 * of the spectrum, this approach initially seems to mean that we would have O(n) complexity in the files where all `
 * the routes have the same stops. But since we're using LinkedHashSet, which is a Set, we should not have any
 * duplications of this hashcode. So still O(1) time complexity when querying. We would have up to O(n^2) when
 * inserting. But realistically, on average we'd be around O(log(n)*n)
 */
@EqualsAndHashCode(exclude = "directBusRoute")
@ToString
public class BusRoute {

    @JsonProperty("dep_sid")
    private Integer depSid;

    @JsonProperty("arr_sid")
    private Integer arrSid;

    @JsonProperty("direct_bus_route")
    private Boolean directBusRoute;
}
