package org.esinecan;

import org.esinecan.config.DataSourceFileWatcher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.loggers.Logger.LOGGER;

@SpringBootApplication
public class Application {

	/**
	 * This is for "closeUsingContext" approach
	 */
	private static ConfigurableApplicationContext applicationContext;

	/**
	 * Sets the data source if present. Starts the app
	 *
	 * @param args Hopefully, first parameter is a plain parameter that points to data source and the rest are options.
	 */
	public static void main(String[] args){
		if(args.length > 0){
			LOGGER.info("Application is starting with arguments: " + args[0]);
			DataSourceFileWatcher.DATA_SOURCE_PATH = args[0]; //FIXME: Not very nice
		}
		try {
			applicationContext = SpringApplication.run(Application.class, args);
			LOGGER.info("Boot complete.");
		}catch (Exception e){
			LOGGER.log(SEVERE, "Application failed to start: ", e);
			shutDown();
		}
	}

	/**
	 * Utilizes application context to shut down the app.
	 */
	public static void shutDown(){
		applicationContext.close();
		LOGGER.info("App shut down.");
	}
}
