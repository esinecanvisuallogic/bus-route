package org.esinecan.integration;

import org.esinecan.integration.base.IntegrationTestBase;
import org.esinecan.model.BusRoute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

public class LinearTimeIntegrationTest extends IntegrationTestBase {

	@Value("${baseUrlLinear}")
	private String baseUrlLinear;

	@Override
	public ResponseEntity<BusRoute> getResponseForParams(Integer depSid, Integer arrSid){
		String url = String.format(baseUrlLinear, depSid, arrSid, "B");
		return restTemplate.getForEntity(url,  BusRoute.class);
	}

}
