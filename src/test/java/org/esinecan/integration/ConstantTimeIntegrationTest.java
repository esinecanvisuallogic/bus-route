package org.esinecan.integration;

import org.esinecan.integration.base.IntegrationTestBase;
import org.esinecan.model.BusRoute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

public class ConstantTimeIntegrationTest extends IntegrationTestBase{

	@Value("${baseUrlConstant}")
	private String baseUrlConstant;

	@Override
	public ResponseEntity<BusRoute> getResponseForParams(Integer depSid, Integer arrSid){
		String url = String.format(baseUrlConstant, depSid, arrSid);
		return restTemplate.getForEntity(url,  BusRoute.class);
	}

}
