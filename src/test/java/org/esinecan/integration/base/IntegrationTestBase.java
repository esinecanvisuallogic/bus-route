package org.esinecan.integration.base;

import org.esinecan.TestBase;
import org.esinecan.model.BusRoute;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 04.05.17.
 */
public abstract class IntegrationTestBase extends TestBase{

    public abstract ResponseEntity<BusRoute> getResponseForParams(Integer depSid, Integer arrSid);

    public TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    public JacksonTester<BusRoute> json;

    @Test
    public void BusRouteDoesNotExist() throws IOException {

        ResponseEntity<BusRoute> response = getResponseForParams(114, 666);

        assertEquals("{\"dep_sid\":114,\"arr_sid\":666,\"direct_bus_route\":false}",
                json.write(response.getBody()).getJson());
    }

    @Test
    public void BusRouteDoesExist() throws IOException {

        ResponseEntity<BusRoute> response = getResponseForParams(114, 169);

        assertEquals("{\"dep_sid\":114,\"arr_sid\":169,\"direct_bus_route\":true}",
                json.write(response.getBody()).getJson());
    }
}
