package org.esinecan.unit;

import org.esinecan.TestBase;
import org.esinecan.config.DataSourceFileWatcher;
import org.esinecan.model.BusRoute;
import org.esinecan.service.busroute.implementation.constanttime.reader.ConstantTimeRouteReader;
import org.junit.Test;

import java.io.IOException;
import java.util.LinkedHashSet;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by eren.sinecan on 04.05.17.
 */
public class ConstantTimeRouteReaderTest extends TestBase{

    @Test
    public void testReadAllOneBusRoutesIntoMap() throws IOException {

        DataSourceFileWatcher.DATA_SOURCE_PATH =
                ConstantTimeRouteReaderTest.class.getClassLoader().getResource("data/example").getPath();

        ConstantTimeRouteReader constantTimeRouteReader = new ConstantTimeRouteReader();
        constantTimeRouteReader.readAllOneBusRoutesIntoMap();
        LinkedHashSet set = constantTimeRouteReader.routes;

        BusRoute busRoute = new BusRoute(153, 142, null);
        assertFalse(set.contains(busRoute));

        busRoute = new BusRoute(5, 150, null);
        assertFalse(set.contains(busRoute));

        busRoute = new BusRoute(114, 121, null);
        assertFalse(set.contains(busRoute));

        busRoute = new BusRoute(114, 17, null);
        assertTrue(set.contains(busRoute));

        busRoute = new BusRoute(121, 148, null);
        assertTrue(set.contains(busRoute));

        busRoute = new BusRoute(142, 106, null);
        assertTrue(set.contains(busRoute));
    }
}
