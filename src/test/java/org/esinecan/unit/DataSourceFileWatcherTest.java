package org.esinecan.unit;

import org.esinecan.TestBase;
import org.esinecan.config.DataSourceFileWatcher;
import org.esinecan.model.BusRoute;
import org.esinecan.service.util.RestartApplicationService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 04.05.17.
 */
public class DataSourceFileWatcherTest extends TestBase {
    @Value("${baseUrlConstant}")
    private String baseUrl;

    public TestRestTemplate restTemplate = new TestRestTemplate();

    @Autowired
    public JacksonTester<BusRoute> json;

    @Test
    public void testRestart() throws IOException {

        BufferedReader file = new BufferedReader(new FileReader(DataSourceFileWatcher.DATA_SOURCE_PATH));

        String line;
        StringBuffer inputBuffer = new StringBuffer();

        while ((line = file.readLine()) != null) {
            inputBuffer.append(line);
            inputBuffer.append('\n');
        }
        String inputStr = inputBuffer.toString();

        file.close();

        inputStr = inputStr.replace("666", "155");

        FileOutputStream fileOut = new FileOutputStream(DataSourceFileWatcher.DATA_SOURCE_PATH);
        fileOut.write(inputStr.getBytes());
        fileOut.close();

        try{
            assertTrue(RestartApplicationService.restartCount == 0);
            ResponseEntity<BusRoute> response = getResponseForParams(114, 666);

            assertEquals("{\"dep_sid\":114,\"arr_sid\":666,\"direct_bus_route\":false}",
                    json.write(response.getBody()).getJson());

            String newInputStr = inputStr.replace("155", "666");
            fileOut = new FileOutputStream(DataSourceFileWatcher.DATA_SOURCE_PATH);
            fileOut.write(newInputStr.getBytes());
            fileOut.close();

            Thread.sleep(10000);

            response = getResponseForParams(114, 666);

            assertEquals("{\"dep_sid\":114,\"arr_sid\":666,\"direct_bus_route\":true}",
                    json.write(response.getBody()).getJson());

            assertTrue(RestartApplicationService.restartCount == 1);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            fileOut = new FileOutputStream(DataSourceFileWatcher.DATA_SOURCE_PATH);
            fileOut.write(inputStr.getBytes());
            fileOut.close();
        }
    }

    public ResponseEntity<BusRoute> getResponseForParams(Integer depSid, Integer arrSid){
        String url = String.format(baseUrl, depSid, arrSid);
        return restTemplate.getForEntity(url,  BusRoute.class);
    }
}
