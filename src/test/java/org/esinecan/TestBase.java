package org.esinecan;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;
import static org.esinecan.config.loggers.Logger.LOGGER;

/**
 * Created by eren.sinecan on 04.05.17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.yml")
@JsonTest
public abstract class TestBase {

    @BeforeClass
    public static void runApp(){
        try {
            String[] args = {};
            Application.main(args);
            LOGGER.log(INFO, "Application started");
        } catch (Exception e) {
            LOGGER.log(SEVERE, "Application failed to start. Try running mvn package. " +
                    "Make sure you have internet connection. Here is what went wrong: {}", e);
        }
    }

    @AfterClass
    public static void shutDownApp(){
        try {
            Application.shutDown();
            LOGGER.log(INFO, "Application stopped");
        } catch (Exception e) {
            LOGGER.log(SEVERE, "Application failed to stop. Port 8088 might be hogged. " +
                    "Try running \"lsof -n -i4TCP:8088 | grep LISTEN\" to see if that's the case. " +
                    "Here is what went wrong: {}", e);
        }
    }

    @Test
    public void contextLoads() {
    }
}
